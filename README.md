# README #

Curso maestro de Django de UDEMY.

## Algunos links de utilidad.

* [Github - web-personal-curso-django-2](https://github.com/hcosta/web-personal-curso-django-2)

* [Github - Repositorio Web Empresarial en Github](https://github.com/hcosta/web-empresa-curso-django-2)

* [Repositorio Web Social en Github (descartada)](https://github.com/hcosta/web-social-curso-django-2)

* [Web oficial de Django Project](https://www.djangoproject.com/)

